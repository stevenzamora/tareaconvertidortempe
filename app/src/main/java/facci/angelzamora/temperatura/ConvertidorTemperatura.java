package facci.angelzamora.temperatura;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ConvertidorTemperatura extends Activity {
    EditText EditTextnumero;
    Button btncelsius, btnfahrenheit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor_temperatura);
        EditTextnumero = (EditText) findViewById(R.id.EditTextingreso);
        btncelsius = (Button) findViewById(R.id.btncelsius);
        btnfahrenheit = (Button) findViewById(R.id.btnfahre);

        btncelsius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero = Integer.parseInt(EditTextnumero.getText().toString());
                float resultadonumero = (float) ((numero * 1.8) + 32);
                Toast.makeText(getApplicationContext(), "La conversión es de: " + resultadonumero+"°F", Toast.LENGTH_SHORT).show();

            }
        });
        btnfahrenheit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero = Integer.parseInt(EditTextnumero.getText().toString());
                float resultadonumero = (float) ((numero - 32)/(1.8));
                Toast.makeText(getApplicationContext(), "La conversión es de: " + resultadonumero+"°C", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

